# QAT Bootstrap Tools

Intel QAT Setup, Test, and Integration utility.  This Makefile and associated configuraiton files bring together the source code, frameworks, and binaries required to use the Intel QAT environment for QAT-ZIP, QAT OpenSSL Engine, or other applications.  The build and installation steps are configured with sensible defaults to function with various QAT cards.

## Dependencies

`make` is used to drive all targets, builds, and intallations.  Some targets require `root` or `sudo` capabilities in order to function.  `maven`, `cpp` and other `autotools` are required to build the packages.

### Install Maven 3.3+

`% make maven`

Target will install `maven` at /opt/maven. `source /etc/profile.d/maven.sh` to use new version, requires `root` or `sudo`

### Download Targets

`$ make get`

Target will download all QAT packages, Hadoop, Spark, and OpenSSL.  This is the default target.

### Build Environment Options

* `QAT_HW` :: QAT Hardware Type, either 'c6xx' or 'dh895xcc'
* `QAT_GROUP` :: System group for QAT device access, default is `qat`
* `QAT_ZIP_OPT` :: QAT ZIP Optimization, either 'thread' or 'process'
* `QAT_ENGINE_OPT` :: QAT SSL Engine Optimization, one of: 'process_event-driven', 'process', 'thread_event-driven', 'thread'

### Versions

See the `Makefile` to override specific versions or tags of git repositories.

## System Targets

> System targets are used to modify system-wide configuration and should be used with care.  Some targets require `install`ed products to function.  All system targets require `root` or `sudo`.

`% make module`

Load the user-space QAT driver with huge pages enabled

`% make qat-group`

Change the `/dev/qat*` access to allow `QAT_GROUP` access.  Users may be allowed to access the QAT device if they are a member of this group: `usermod -a -G qat username`

`% make restart`

Restart the QAT engine and apply the `qat-group` target changes

`% make status`

Display the current status of the QAT card

### Configure for Use <a name="cfu"></a>

`% make use-qzip`

Reconfigure the QAT environment to be used for the QZip utility

`% make use-engine`

Reconfigure the QAT environment to be used for the QAT OpenSSL Engine

## Build Targets

`$ make build`

Invoke all following `build` targets

`$ make build-qat`

Build the QAT Drivers

`$ make build-qzip`

Build the QAT ZIP utility, used by Codec

`$ make build-codec`

Build the QAT Hadoop and Spark accelerators.  Implies `make build-qzip`

`$ make build-engine`

Build the QAT SSL engine, implies `make build-openssl`

`$ make build-openssl`

Build the OpenSSL version required by QAT Engine

## Test Targets

`$ make test-codec`

Run all included tests for the QAT Codec project

`$ make test-engine`

Test OpenSSL engine installation for QAT.  Must return an `available` status.  See `use-engine` target to properly set up QAT Engine environment.

`$ make test-engine-speed`

Test OpenSSL engine speeds with QAT.  Must return an `available` status from `test-engine` target.  See `use-engine` target to properly set up QAT Engine environment.

`$ make test-qzip`

Test the speed comparisons between QAT ZIP and GZip

## Install Targets

> Install targets require `root` or `sudo`.

`% make backup`

Backup all QAT configuraitons defined by `QAT_HW` located in `/etc` to `./backup`

`% make install`

Invoke all following `install` targets (except codec)

`% make install-openssl`

Install the OpenSSL library to `OPENSSL_INSTALL`

`% make install-qat`

Install the QAT library to the default location

`% make install-engine`

Install the QAT SSL Engine accelerator to the OpenSSL location

`% make install-qzip`

Install the QAT ZIP utilities to the default location

`% make install-codec`

Install the Hadoop and Spark accelerators to the appropriate locations

### Configuration Installation

> Instead of using these targets, consider using the [Configure for Use](#cfu) targets listed above.

`% make install-qzip-conf`

Apply the QAT configuration files required for QAT ZIP acceleration.  See `QAT_HW` and `QAT_ZIP_OPT`.

`% make install-engine-conf`

Apply the QAT configuration files required for QAT SSL acceleration.  See `QAT_HW` and `QAT_ENGINE_OPT`.

## Utility Targets

`$ make clean`

Clean all downloaded directories, may be run with `sudo` for any permissions conflicts

`$ make distclean`

Remove all downloaded directories and archives.  Intended to reset the project to a default configuration.

## Warranty

MIT License

Copyright (c) 2019 Damon Brown

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

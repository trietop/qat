.PHONY: clean distclean conf all maven backup install-conf useroot

### Versions
OPENSSL_VERSION := OpenSSL_1_1_1b
MAVEN_VERSION := 3.6.3
HADOOP_VERSION := 3.2.1
SPARK_VERSION := 2.4.3
SPARK_RDMA_LIB := 2.4.0
SPARK_RDMA_VERSION := 3.1

SPARK_BENCH_VERSION := v99/spark-bench_2.3.0_0.4.0-RELEASE_99

ISA_L_VERSION := master
QAT_DRIVER_VERSION := 1.7.l.4.9.0-00008
QAT_ENGINE_VERSION := master
QAT_ZIP_VERSION := master
QAT_CODEC_VERSION := master

# System group for QAT access
QAT_GROUP := qat
# QAT HW is either 'c6xx' or 'dh895xcc'
QAT_HW := c6xx
# QAT_ZIP_OPT is either 'thread' or 'process'
QAT_ZIP_OPT := thread
# QAT_ENGINE_OPT is one of: 'process_event-driven', 'process', 'thread_event-driven', 'thread'
QAT_ENGINE_OPT := process_event-driven
SSL_ROOT := /usr/local/ssl
ICP_ROOT := /usr/lib/modules/$(shell uname -r)/kernel/drivers

OPENSSL_INSTALL := /usr/local/ssl
OPENSSL_ENGINES := $(OPENSSL_INSTALL)/lib/engines-1.1

SYSTEM_LIB := /usr/local/lib64
QAT_ROOT := $(shell pwd)

all: get

### Cleanup
clean:
	rm -rf qat qatzip qatcodec qatengine openssl hadoop spark

perms: useroot
	chown -R $(USER) $(QAT_ROOT)

distclean: clean
	rm -rf archive

### Configuration
versions:
	@echo
	@echo QAT Version:	$(QAT_DRIVER_VERSION)
	@echo QATZip Version:	$(QAT_ZIP_VERSION)
	@echo QATCodec Version:	$(QAT_CODEC_VERSION)
	@echo
	@echo QAT Hardware: 	$(QAT_HW)
	@echo
	@echo Huge pages [2048k]: `cat /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages`

maven: useroot
	wget https://www-us.apache.org/dist/maven/maven-3/$(MAVEN_VERSION)/binaries/apache-maven-$(MAVEN_VERSION)-bin.tar.gz -P /tmp
	tar xf /tmp/apache-maven-$(MAVEN_VERSION)-bin.tar.gz -C /opt
	ln -s /opt/apache-maven-$(MAVEN_VERSION) /opt/maven
	cp maven.sh /etc/profile.d/maven.sh && chmod +x /etc/profile.d/maven.sh
	@echo 'source /etc/profile.d/maven.sh' to use new version

useroot:
	@if ! [ "$(shell id -u)" = 0 ]; then echo "You are not root, run this target as root please"; exit 1; fi

### Projects
get: archive/qat.tgz qatzip qatcodec qatengine openssl hadoop spark

archive:
	mkdir -p archive

archive/qat.tgz: | archive
	wget -O archive/qat.tgz --no-use-server-timestamps \
		https://01.org/sites/default/files/downloads/qat$(QAT_DRIVER_VERSION).tar.gz

qat: archive/qat.tgz
	mkdir -p qat archive
	tar -zxf archive/qat.tgz -C qat

qatengine:
	git clone https://github.com/01org/QAT_Engine qatengine
ifneq ($(QAT_ENGINE_VERSION), master)
	cd qatengine && git checkout -b $(QAT_ENGINE_VERSION) $(QAT_ENGINE_VERSION)
endif

qatzip:
	git clone https://github.com/intel/QATzip qatzip
ifneq ($(QAT_ZIP_VERSION), master)
	cd qatzip && git checkout -b $(QAT_ZIP_VERSION) $(QAT_ZIP_VERSION)
endif

qatcodec:
	git clone https://github.com/Intel-bigdata/IntelQATCodec qatcodec
ifneq ($(QAT_CODEC_VERSION), master)
	cd qatcodec && git checkout -b $(QAT_CODEC_VERSION) $(QAT_CODEC_VERSION)
endif

openssl:
	git clone https://github.com/openssl/openssl openssl
ifneq ($(OPENSSL_VERSION), master)
	cd openssl && git checkout -b $(OPENSSL_VERSION) $(OPENSSL_VERSION)
endif

archive/hadoop.tgz: | archive
	wget -O archive/hadoop.tgz --no-use-server-timestamps \
		https://www-us.apache.org/dist/hadoop/common/hadoop-$(HADOOP_VERSION)/hadoop-$(HADOOP_VERSION).tar.gz

hadoop: archive/hadoop.tgz
	tar -zxf archive/hadoop.tgz
	mv hadoop-$(HADOOP_VERSION) hadoop
	@touch hadoop

hadoop-env: hadoop
	@echo export PATH=${PATH}:$(QAT_ROOT)/hadoop/bin:$(QAT_ROOT)/hadoop/sbin

archive/spark.tgz: | archive
	wget -O archive/spark.tgz --no-use-server-timestamps \
		https://archive.apache.org/dist/spark/spark-$(SPARK_VERSION)/spark-$(SPARK_VERSION)-bin-without-hadoop.tgz

archive/spark-rdma.tgz: | archive
	wget -O archive/spark-rdma.tgz --no-use-server-timestamps\
		https://github.com/Mellanox/SparkRDMA/releases/download/$(SPARK_RDMA_VERSION)/spark-rdma-$(SPARK_RDMA_VERSION).tgz

archive/spark-bench.tgz: | archive
	wget -O archive/spark-bench.tgz --no-use-server-timestamps \
		https://github.com/CODAIT/spark-bench/releases/download/$(SPARK_BENCH_VERSION).tgz

spark: | archive/spark.tgz
	tar -zxf archive/spark.tgz
	mv spark-$(SPARK_VERSION)-bin-without-hadoop spark
	@touch spark

spark-bench: | archive/spark-bench.tgz
	tar -zxf archive/spark-bench.tgz
	mv spark-bench_2.3.0_0.4.0-RELEASE spark-bench
	@touch spark-bench

spark-rdma: | archive/spark-rdma.tgz
	mkdir -p spark-rdma
	tar zxf archive/spark-rdma.tgz -C spark-rdma

spark-env: spark
	@echo export PATH=${PATH}:$(QAT_ROOT)/spark/bin:$(QAT_ROOT)/spark/sbin

isa-l:
	@echo Requires 'yasm' to be installed
	git clone https://github.com/01org/isa-l.git isa-l
ifneq ($(ISA_L_VERSION), master)
	cd isa-l && git checkout -b $(ISA_L_VERSION) $(ISA_L_VERSION)
endif

### Build Scripts
build: build-qat build-qzip build-codec build-engine

build-qat: qat/build/usdm_drv.ko

build-qzip: qatzip/utils/qzip

build-codec: | qatzip/utils/qzip qatcodec/hadoop_qat_wrapper/target qatcodec/spark_qat_wrapper/target

build-engine: openssl/apps/openssl qatengine/.libs/libqat.so

build-isa-l: isa-l/bin/libisal.so

build-openssl: openssl/apps/openssl

isa-l/bin/libisal.so: $(filter-out $(wildcard isa-l), isa-l)
	cd isa-l && $(MAKE) -f Makefile.unx

openssl/Makefile: $(filter-out $(wildcard openssl), openssl)
	cd openssl && ./config --prefix=$(SSL_ROOT) -Wl,-rpath,$(SSL_ROOT)/lib

openssl/apps/openssl: openssl/Makefile
	cd openssl && $(MAKE)

qat/Makefile: $(filter-out $(wildcard qat), qat)
	cd qat && ./configure

qat/build/usdm_drv.ko: qat/Makefile
	cd qat && $(MAKE)

qatengine/Makefile: $(filter-out $(wildcard qatengine), qatengine)
	cd qatengine && ./configure --enable-usdm --enable-upstream_driver \
		--with-qat_dir=$(QAT_ROOT)/qat --with-openssl_dir=$(QAT_ROOT)/openssl \
		--with-openssl_install_dir=/usr/local/ssl

qatengine/.libs/libqat.so: qatengine/Makefile
	cd qatengine && PERL5LIB=${PERL5LIB}:$(QAT_ROOT)/openssl/ $(MAKE)

qatzip/Makefile: qat/build/usdm_drv.ko $(filter-out $(wildcard qatzip), qatzip)
	cd qatzip && ./configure --with-ICP_ROOT=$(QAT_ROOT)/qat

qatzip/utils/qzip: qatzip/Makefile
	cd qatzip && $(MAKE) all

qatcodec/hadoop_qat_wrapper/target: $(filter-out $(wildcard qatcodec), qatcodec)
	cd qatcodec && mvn clean install -Dqatzip.libs=$(QAT_ROOT)/qatzip/src \
		-Dqatzip.src=$(QAT_ROOT)/qatzip -pl hadoop_qat_wrapper

qatcodec/spark_qat_wrapper/target: $(filter-out $(wildcard qatcodec), qatcodec)
	cd qatcodec && mvn clean install -Dqatzip.libs=$(QAT_ROOT)/qatzip/src \
		-Dqatzip.src=$(QAT_ROOT)/qatzip -pl spark_qat_wrapper

### Install Scripts
backup: useroot
	mkdir -p backup
	cp /etc/$(QAT_HW)*.conf $(QAT_ROOT)/backup

install: install-conf install-qat install-qzip install-openssl

install-openssl: useroot openssl/apps/openssl
	cd openssl && $(MAKE) install

install-isa-l: useroot isa-l/bin/libisal.so
	cd isa-l && $(MAKE) -f Makefile.unx install

#./qatzip/config_file/c6xx/multiple_process_opt/c6xx_dev0.conf
#./qatzip/config_file/c6xx/multiple_process_opt/c6xx_dev1.conf
#./qatzip/config_file/c6xx/multiple_process_opt/c6xx_dev2.conf
#./qatzip/config_file/c6xx/multiple_thread_opt/c6xx_dev0.conf
#./qatzip/config_file/c6xx/multiple_thread_opt/c6xx_dev1.conf
#./qatzip/config_file/c6xx/multiple_thread_opt/c6xx_dev2.conf
#./qatzip/config_file/dh895xcc/multiple_process_opt/dh895xcc_dev0.conf
#./qatzip/config_file/dh895xcc/multiple_thread_opt/dh895xcc_dev0.conf
install-qzip-conf: useroot
	cp -vf $(QAT_ROOT)/qatzip/config_file/$(QAT_HW)/multiple_$(QAT_ZIP_OPT)_opt/*.conf /etc/

#./qat/config/c2xxx/multi_process_optimized/c2xxx_qa_dev0.conf
#./qat/config/c2xxx/multi_process_optimized/c2xxx_qa_dev0_single_ae.conf
#./qat/config/c2xxx/multi_thread_optimized/c2xxx_qa_dev0.conf
#./qat/config/c2xxx/multi_thread_optimized/c2xxx_qa_dev0_single_ae.conf
#./qat/config/c3xxx/multi_process_event-driven_optimized/c3xxx_dev0.conf
#./qat/config/c3xxx/multi_process_optimized/c3xxx_dev0.conf
#./qat/config/c3xxx/multi_thread_event-driven_optimized/c3xxx_dev0.conf
#./qat/config/c3xxx/multi_thread_optimized/c3xxx_dev0.conf
#./qat/config/c6xx/multi_process_event-driven_optimized/c6xx_dev0.conf
#./qat/config/c6xx/multi_process_event-driven_optimized/c6xx_dev1.conf
#./qat/config/c6xx/multi_process_event-driven_optimized/c6xx_dev2.conf
#./qat/config/c6xx/multi_process_optimized/c6xx_dev0.conf
#./qat/config/c6xx/multi_process_optimized/c6xx_dev1.conf
#./qat/config/c6xx/multi_process_optimized/c6xx_dev2.conf
#./qat/config/c6xx/multi_thread_event-driven_optimized/c6xx_dev0.conf
#./qat/config/c6xx/multi_thread_event-driven_optimized/c6xx_dev1.conf
#./qat/config/c6xx/multi_thread_event-driven_optimized/c6xx_dev2.conf
#./qat/config/c6xx/multi_thread_optimized/c6xx_dev0.conf
#./qat/config/c6xx/multi_thread_optimized/c6xx_dev1.conf
#./qat/config/c6xx/multi_thread_optimized/c6xx_dev2.conf
#./qat/config/dh895xcc/multi_process_event-driven_optimized/dh895xcc_qa_dev0.conf
#./qat/config/dh895xcc/multi_process_optimized/dh895xcc_qa_dev0.conf
#./qat/config/dh895xcc/multi_thread_event-driven_optimized/dh895xcc_qa_dev0.conf
#./qat/config/dh895xcc/multi_thread_optimized/dh895xcc_qa_dev0.conf
#./qat/config/dh895xcc_upstream/multi_process_event-driven_optimized/dh895xcc_dev0.conf
#./qat/config/dh895xcc_upstream/multi_process_optimized/dh895xcc_dev0.conf
#./qat/config/dh895xcc_upstream/multi_thread_event-driven_optimized/dh895xcc_dev0.conf
#./qat/config/dh895xcc_upstream/multi_thread_optimized/dh895xcc_dev0.conf
#./qat/config/dh89xxcc/multi_process_optimized/dh89xxcc_qa_dev0.conf
#./qat/config/dh89xxcc/multi_process_optimized/dh89xxcc_qa_dev0_single_accel.conf
#./qat/config/dh89xxcc/multi_process_optimized/dh89xxcc_qa_dev1.conf
#./qat/config/dh89xxcc/multi_process_optimized/dh89xxcc_qa_dev1_single_accel.conf
#./qat/config/dh89xxcc/multi_thread_optimized/dh89xxcc_qa_dev0.conf
#./qat/config/dh89xxcc/multi_thread_optimized/dh89xxcc_qa_dev0_single_accel.conf
#./qat/config/dh89xxcc/multi_thread_optimized/dh89xxcc_qa_dev1.conf
#./qat/config/dh89xxcc/multi_thread_optimized/dh89xxcc_qa_dev1_single_accel.conf
install-engine-conf: useroot
	cp -vf $(QAT_ROOT)/qatengine/qat/config/$(QAT_HW)/multi_$(QAT_ENGINE_OPT)_optimized/*.conf /etc/

install-qat: useroot qat/build/usdm_drv.ko
	cd qat && $(MAKE) install

install-engine: useroot qatengine/.libs/libqat.so
	cd qatengine && $(MAKE) install

install-qzip: useroot qatzip/utils/qzip
	cd qatzip && $(MAKE) install
	@echo Making libqatzip World Readable
	chmod a+rx $(SYSTEM_LIB)/libqatzip*

install-hadoop-conf: | hadoop
	cp -vf $(QAT_ROOT)/conf/hadoop/* $(QAT_ROOT)/hadoop/etc/hadoop/

install-spark-conf: | spark
	cp -vf $(QAT_ROOT)/conf/spark/* $(QAT_ROOT)/spark/conf/

install-conf: | install-hadoop-conf install-spark-conf

install-codec: | install-conf build-codec
	@echo Copying Hadoop libraries
	cp -vf $(QAT_ROOT)/qatcodec/hadoop_qat_wrapper/target/hadoop_qat_wrapper-1.0.0.jar $(QAT_ROOT)/hadoop/share/hadoop/common/
	cp -vf $(QAT_ROOT)/qatcodec/hadoop_qat_wrapper/target/libqatcodec.so $(QAT_ROOT)/hadoop/lib/native/
	@echo
	@echo Copying Spark libraries
	cp -vf $(QAT_ROOT)/qatcodec/spark_qat_wrapper/target/spark_qat_wrapper-1.0.0.jar $(QAT_ROOT)/spark/jars/

install-rdma: | useroot spark-rdma
	cp -vf spark-rdma/libdisni.so $(SYSTEM_LIB)/
	@echo Now run 'install-spark-rdma' as non-root user

install-spark-rdma:
	cp -vf spark-rdma/spark-rdma-$(SPARK_RDMA_VERSION)-for-spark-$(SPARK_RDMA_LIB)-jar-with-dependencies.jar spark/jars

### Tests
test-codec:
	cd qatcodec && mvn clean test -Dqatzip.libs=$(QAT_ROOT)/qatzip/src \
		-Dqatzip.src=$(QAT_ROOT)/qatzip -pl \!carbondata_qat_wrapper

test-engine: useroot
	$(OPENSSL_INSTALL)/bin/openssl engine -t -c -vvvv qat

test-engine-speed: useroot
	@echo RSA 2K
	@echo Asynchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed -async_jobs 72 rsa2048
	@echo Synchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed rsa2048
	@echo Software
	$(OPENSSL_INSTALL)/bin/openssl speed -elapsed rsa2048
	@echo
	@echo ---
	@echo
	@echo ECDH Compute Key
	@echo Asynchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed -async_jobs 36 ecdh
	@echo Synchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed ecdh
	@echo Software
	$(OPENSSL_INSTALL)/bin/openssl speed -elapsed ecdh
	@echo
	@echo ---
	@echo
	@echo Chained Cipher: aes-128-cbc-hmac-sha1
	@echo Asynchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed -async_jobs 128 -multi 2 -evp aes-128-cbc-hmac-sha1
	@echo Synchronous
	$(OPENSSL_INSTALL)/bin/openssl speed -engine qat -elapsed -multi 2 -evp aes-128-cbc-hmac-sha1
	@echo Software
	$(OPENSSL_INSTALL)/bin/openssl speed -elapsed -multi 2 -evp aes-128-cbc-hmac-sha1

test-file:
	dd if=/dev/urandom bs=1 count=10000000 | base64 > test-file

test-qzip-file: test-file
	@echo
	@echo Running with 'qzip'
	time qzip test-file
	@echo
	time qzip -d test-file.gz

test-gzip-file: test-file
	@echo
	@echo Running with 'gzip'
	@echo Compress
	time gzip -v test-file
	@echo
	@echo Decompress
	time gzip -dv test-file.gz

test-qzip: | test-qzip-file test-gzip-file

### System Utilities
module: useroot
	-rmmod -f usdm_drv
	echo 2048 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages
	insmod $(ICP_ROOT)/usdm_drv.ko max_huge_pages=1024 max_huge_pages_per_process=16

system:

qat-group: useroot
	chgrp $(QAT_GROUP) /dev/qat_* /dev/uio* /dev/usdm_drv
	chmod 660 /dev/qat_* /dev/uio* /dev/usdm_drv

use-qzip: | useroot install-qzip-conf restart

use-engine: | useroot install-engine-conf restart

restart: | useroot service-restart qat-group

service-restart: useroot
	service qat_service restart

status: useroot
	service qat_service status
